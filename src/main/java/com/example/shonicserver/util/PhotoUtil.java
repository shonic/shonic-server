package com.example.shonicserver.util;

import com.example.shonicserver.configuration.ConstantConfig;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class PhotoUtil {
    private static final Path root = Paths.get("uploads");
    private static final Path pathPayment = Paths.get("uploads/payment");

    public static String getFileExtension(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String[] fileNameSplit = fileName.split("\\.");//split between name and extension
        return fileNameSplit[fileNameSplit.length - 1];
    }

    public static String uploadImagePayment(MultipartFile file, String code, String fileNameFormat) throws IOException {
        if (file != null && !file.isEmpty()) {
            fileNameFormat = fileNameFormat + "." + getFileExtension(file);
            try {
                Files.createDirectory(pathPayment);
            }catch (Exception e){
                System.out.println("Folder Already Created");
            }

            Files.copy(file.getInputStream(),pathPayment.resolve(fileNameFormat));
            return fileNameFormat;
        }
        return null;
    }
    public static Resource loadImagePayment(String filename) {
        try {
            Path file = pathPayment.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
    public static Map<String, String> listDirectory() {
        Map<String, String> result = new HashMap<>();
        result.put("payment","payment");
        return result;
    }
}
