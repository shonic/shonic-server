package com.example.shonicserver.util;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

public class CommonFunction {

    public static String getDateTime(LocalDateTime date) {
        return getDateFrom(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String getDateFrom(LocalDateTime date, String format) {
        if (date == null) return "";
        return date.format(DateTimeFormatter.ofPattern(format));
    }

    public static String getPayBefore(LocalDateTime date){
        String day = getDateFrom(date, "E");
        day = convertDay(day);
        String m = connverMonth(getDateFrom(date,"MM"));
        String d = getDateFrom(date,"dd");
        return day+", "+d+" "+m+" "+getDateFrom(date,"yyyy HH:mm");
    }
    public static String getDateOrder(LocalDateTime date){
        String day = getDateFrom(date, "E");
        day = convertDay(day);
        String m = connverMonth(getDateFrom(date,"MM"));
        String d = getDateFrom(date,"dd");
        return day+", "+d+" "+m+" "+getDateFrom(date,"yyyy | HH:mm");
    }

    public static String getBeforeRate(LocalDateTime date){
        String m = connverMonth(getDateFrom(date,"MM"));
        String d = getDateFrom(date,"dd");
        return d+" "+m+" "+getDateFrom(date,"yyyy");
    }
    public static String generateRandom(String text) {
        try {
            return Encryption.SHA1(new Date() + generateRandomString(10) + text);
        } catch (NoSuchAlgorithmException e) {
            return generateRandomString(20);
        }
    }
    public static String generateRandomString(int length) {
        Random rd = new Random();
        String res = "";
        int count = 0;
        while (count < length) {
            String variation = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            res += variation.charAt(rd.nextInt(variation.length()));
            count++;
        }
        return res;
    }

    private static String convertDay(String day){
        switch (day){
            case "Sun":
                return "Minggu";
            case "Mon":
                return "Senin";
            case "Tue":
                return "Selasa";
            case "Thu":
                return "Rabu";
            case "Wed":
                return "Kamis";
            case "Fri":
                return "Jumat";
            case "Sat":
                return "Sabtu";
            default:return "";

        }
    }

    public static String connverMonth(String month){
        switch (month){
            case "01":
                return "Januari";
            case "02":return "Februari";
            case "03":return "Maret";
            case "04":return "April";
            case "05":return "Mei";
            case "06":return "Juni";
            case "07":return "Juli";
            case "08":return "Agustus";
            case "09":return "September";
            case "10":return "Oktober";
            case "11":return "September";
            case "12":return "Desember";
            default:return "";
        }
    }

    public static String numberFormatWithoutComma(Double amount) {
        return amount==null?"0":String.format("%,.0f", amount);
    }

    public static String curenyFormat(Double amount){
        return "Rp. "+numberFormatWithoutComma(amount);
    }


}
