package com.example.shonicserver.controller;

import com.example.shonicserver.dto.ProfileDto;
import com.example.shonicserver.model.Profile;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@CrossOrigin(origins = "*")
@Api(tags = "User")
public class UserController {
    @Autowired
    UserService userService;

    @DeleteMapping("/delete")
    public ResponseEntity<Response> delete(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        System.out.println("Email " + userDetails.getUsername());
        User user=this.userService.deletedUser(userDetails.getUsername());
        return new ResponseEntity<>(new Response(200,"success",user,null), HttpStatus.OK);
    }
    @PostMapping("/updateProfile")
    public ResponseEntity<Response> updateProfile(@RequestBody ProfileDto profileDto) {
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) authentication.getPrincipal();
        ProfileDto profileUpdate = this.userService.updateProfile(profileDto,userDetails.getUsername());

        return new ResponseEntity(new Response(201,"success",profileUpdate,null), HttpStatus.CREATED);
    }
    @GetMapping("/getProfileByEmail")
    public ResponseEntity<Response> updateProfileByemail() {
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) authentication.getPrincipal();
        ProfileDto profileUpdate = this.userService.getProfileByUsername(userDetails.getUsername());
        return new ResponseEntity(new Response(201,"success",profileUpdate,null), HttpStatus.CREATED);
    }
}
