package com.example.shonicserver.controller;

import com.example.shonicserver.model.City;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.payload.request.AddressRequest;
import com.example.shonicserver.payload.response.AddressResponse;
import com.example.shonicserver.service.AddresService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/address")
@Api(tags = "Address")
@CrossOrigin(origins = "*")
public class AddressController {
    @Autowired
    private AddresService addresService;

    @GetMapping("/provinces")
    public ResponseEntity<Response> getAllProvinces(){
        return new ResponseEntity<>(new Response(200,"Success",addresService.getAllProvince(),null), HttpStatus.OK);
    }

    @GetMapping("/cities")
    public ResponseEntity<Response> getAllCities(){
        return new ResponseEntity<>(new Response(200,"Success",addresService.getAllCity(),null), HttpStatus.OK);
    }

    @GetMapping("/province/{id}/cities")
    public ResponseEntity<Response> getAllCitiesByProvince(@PathVariable Long id){
        List<City> cityList = addresService.getAllCityByProvince(id);
        return new ResponseEntity<>(new Response(200,"Success",cityList,null), HttpStatus.OK);
    }
    @GetMapping("/myaddress")
    public ResponseEntity<Response> getMyAddress(){
        return new ResponseEntity<>(new Response(200,"Success",addresService.getAddress(getEmailLogin()),null), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveAddress(@RequestBody AddressRequest data){
        AddressResponse address =addresService.saveAddress(data,getEmailLogin());
        if(address != null){
            return new ResponseEntity<>(new Response(200,"Success",address,null), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new Response(400,"Failed",null,null), HttpStatus.BAD_REQUEST);
        }
    }

    private String getEmailLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
