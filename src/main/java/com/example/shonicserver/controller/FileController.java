package com.example.shonicserver.controller;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.service.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/images")
@Api(tags = "Image")
@CrossOrigin("*")
public class FileController {
    @Autowired
    private OrderService orderService;

    private final String pathDefault = "uploads/";

    @GetMapping(value = "/payment/{name}")
    public @ResponseBody ResponseEntity<InputStreamResource> getImage(HttpServletRequest request, @PathVariable("name") String filename) throws IOException, BadRequestException {
        String path = pathDefault.concat("payment/"+filename);
        Path pathPayment;
        File file;
        try {
            pathPayment = Paths.get(path);
            file = new File(path);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(Files.probeContentType(pathPayment)))
                    .body(new InputStreamResource(new FileInputStream(file)));
        }catch (Exception e){
            throw new BadRequestException("Image not found");
        }


    }
}
