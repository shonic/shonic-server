package com.example.shonicserver.controller;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.payload.request.ReviewRequest;
import com.example.shonicserver.service.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/order")
@Api(tags = "Order and Payment")
@CrossOrigin("*")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/{id}/payment")
    public ResponseEntity<Response> getPaymentData(@PathVariable Long id) throws BadRequestException {
        return new ResponseEntity<>(new Response(200,"Success",orderService.getPayment(id,getEmailLogin()),null), HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/payment",consumes = {"multipart/form-data"})
    public ResponseEntity<Response> savePayment(@PathVariable Long id,
                                                @RequestParam(value = "bank_name",defaultValue = "")String bankName,
                                                @RequestParam(value = "name",defaultValue = "")String name,
                                                @RequestParam(value = "bank_number",defaultValue = "")String number,
                                                @RequestParam(value = "total_transfer",defaultValue = "")Integer transfer ,
                                                @RequestParam(value = "photo",required = false)MultipartFile file) throws BadRequestException, IOException {
        String message = orderService.savePayment(id,name,bankName,number,transfer,file,getEmailLogin());
        if(message.equals("Success"))
        return new ResponseEntity<>(new Response(200,"Success",null,null), HttpStatus.OK);
        else
            return new ResponseEntity<>(new Response(500,"INTERNAL SERVER ERROR",null,null), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/list")
    public ResponseEntity<Response> getOrderList(@RequestParam(value = "pageNo",defaultValue = "1") int pageNo,
                                                 @RequestParam(value = "pageSize",defaultValue = "20") int pageSize,
                                                 @RequestParam(value = "status",defaultValue = "")String status){
        return new ResponseEntity<>(new Response(200,"Success",orderService.getOrderList(pageSize,pageNo,status,getEmailLogin()),null), HttpStatus.OK);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Response> getDetail(@PathVariable Long id) throws BadRequestException {
        return new ResponseEntity<>(new Response(200,"Success",orderService.getDetail(id,getEmailLogin()),null), HttpStatus.OK);
    }

    @PostMapping("/{id}/cancel")
    public ResponseEntity<Response> cancelOrder(@PathVariable Long id) throws BadRequestException {
        orderService.cancelOrder(id,getEmailLogin());
        return new ResponseEntity<>(new Response(200,"Success",null,null), HttpStatus.OK);
    }

    @PostMapping("/{id}/received")
    public ResponseEntity<Response> orderReceived(@PathVariable Long id) throws BadRequestException {
        orderService.receivedOrder(id,getEmailLogin());
        return new ResponseEntity<>(new Response(200,"Success",null,null), HttpStatus.OK);
    }

    @PostMapping("/{id}/review")
    public ResponseEntity<Response> saveReview(@PathVariable Long id, @RequestBody ReviewRequest data) throws BadRequestException {
        orderService.saveReview(id,getEmailLogin(),data);
        return new ResponseEntity<>(new Response(200,"Success",null,null), HttpStatus.OK);
    }

    private String getEmailLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

}
