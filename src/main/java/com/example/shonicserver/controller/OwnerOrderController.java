package com.example.shonicserver.controller;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.service.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/owner/order")
@CrossOrigin(origins = "*")
@Api(tags = "Owner Order")
public class OwnerOrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public ResponseEntity<Response> getOrderList(@RequestParam(value = "pageNo",defaultValue = "1") int pageNo,
                                                 @RequestParam(value = "pageSize",defaultValue = "20") int pageSize,
                                                 @RequestParam(value = "status",defaultValue = "")String status){
        return new ResponseEntity<>(new Response(200,"Success",orderService.getOrderListAdmin(pageSize,pageNo,status),null), HttpStatus.OK);
    }
    @GetMapping("/list/confirm")
    public ResponseEntity<Response> getOrderListConfirm(@RequestParam(value = "pageNo",defaultValue = "1") int pageNo,
                                                 @RequestParam(value = "pageSize",defaultValue = "10") int pageSize){
        return new ResponseEntity<>(new Response(200,"Success",orderService.getOrderListConfirm(pageSize,pageNo),null), HttpStatus.OK);
    }

    @GetMapping("/list/packing")
    public ResponseEntity<Response> getOrderListPacking(@RequestParam(value = "pageNo",defaultValue = "1") int pageNo,
                                                 @RequestParam(value = "pageSize",defaultValue = "10") int pageSize){

        return new ResponseEntity<>(new Response(200,"Success",orderService.getListPacking(pageNo,pageSize),null), HttpStatus.OK);
    }

    @PutMapping("/{id}/payment/confirm")
    public ResponseEntity<Response> updatePayment(@RequestParam("status")Integer status,
                                                  @PathVariable("id")Long id) throws BadRequestException {
        if(status==null)
            return new ResponseEntity<>(new Response(400,"Status Required",null,null), HttpStatus.OK);

        orderService.updatePayment(id,status);
        return new ResponseEntity<>(new Response(200,"Success",id,null), HttpStatus.OK);

    }
    @GetMapping("/detail/{id}")
    public ResponseEntity<Response> getDetail(@PathVariable Long id) throws BadRequestException {
        return new ResponseEntity<>(new Response(200,"Success",orderService.getDetailAdmin(id),null), HttpStatus.OK);
        }

    @PutMapping("/{id}/packing")
    public ResponseEntity<Response> updatePacking(@RequestParam(value = "resi",required = false)String resi,
                                                  @PathVariable("id")Long id) throws BadRequestException {
        if(resi==null)
            return new ResponseEntity<>(new Response(400,"Resi is Required",null,null), HttpStatus.OK);

        orderService.updatePacking(id,resi);
        return new ResponseEntity<>(new Response(200,"Success",id,null), HttpStatus.OK);

    }
    private String getEmailLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
