package com.example.shonicserver.controller;

import com.example.shonicserver.dto.AccountDto;
import com.example.shonicserver.dto.ProductDetailDTO;
import com.example.shonicserver.model.Account;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/account")
@CrossOrigin(origins = "*")
@Api(tags = "Account Bank")
public class AccountController {
    @Autowired
    UserService userService;
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/create")
    private ResponseEntity<Response> insertAccount(@RequestBody AccountDto accountDto){
        Account account=this.userService.createAccount(accountDto);
        return new ResponseEntity<>(new Response(201,"success",account,null), HttpStatus.CREATED);
    }
    @GetMapping("/all")
    public ResponseEntity<List<AccountDto>>getAllAccount(){
        List<Account>accountDtoList=this.userService.getAllAccount();
        return new ResponseEntity(new Response(200,"success",accountDtoList,null), HttpStatus.OK);
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Response> delete(@PathVariable("id") Integer id) {
        AccountDto accountDto= this.userService.delete(id);
            return new ResponseEntity<>(new Response(200,"success",null,null), HttpStatus.OK);


    }
    @GetMapping("getById/{id}")
    public ResponseEntity<Response> getOneAccount(@PathVariable Integer id) {
        Account accountDto=this.userService.getById(id);
        return new ResponseEntity<>(new Response(200,"succsess",accountDto,null),HttpStatus.OK);


    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
   @PostMapping("/updateAccount")
   public ResponseEntity<Response> updateDetail(@RequestBody AccountDto accountDto) {
       Account accountDtoUpdate = this.userService.updateAccount(accountDto);

       return new ResponseEntity(new Response(201,"success",accountDtoUpdate,null), HttpStatus.CREATED);
   }


}
