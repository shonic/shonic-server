package com.example.shonicserver.controller;

import com.example.shonicserver.dto.JwtResponseDto;
import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.model.ERole;
import com.example.shonicserver.model.Payment;
import com.example.shonicserver.model.Role;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.response.AdminOrderConfirmResponse;
import com.example.shonicserver.payload.response.AdminOrderDetail;
import com.example.shonicserver.payload.response.AdminOrderPackingResponse;
import com.example.shonicserver.payload.response.OrderResponse;
import com.example.shonicserver.repository.OrderRepository;
import com.example.shonicserver.repository.PaymentRepository;
import com.example.shonicserver.repository.ProductRepository;
import com.example.shonicserver.repository.UserRepository;
import com.example.shonicserver.service.JpaUserDetailsService;
import com.example.shonicserver.service.OrderService;
import com.example.shonicserver.service.UserService;
import com.example.shonicserver.util.CommonFunction;
import com.example.shonicserver.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping("/cms")
public class DashboardController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JpaUserDetailsService jpaUserDetailsService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private PaymentRepository paymentRepository;

    @GetMapping("")
    public String home(){
        return "redirect:./cms/dashboard";
    }
    @GetMapping("/login")
    public String loginPage(HttpServletRequest request) {
        String tokenCookie = null;
        try {
            tokenCookie = Arrays.stream(request.getCookies())
                    .filter(cookie ->cookie.getName().equals("token")).findFirst().map(Cookie::getValue)
                    .orElse(null);
        }catch (Exception e){
            tokenCookie=null;
        }
        if(tokenCookie!=null){
            return "redirect:./dashboard";
        }else{
            return "auth-login";
        }

    }

    @PostMapping("/login")
    public String login(@RequestParam String email, @RequestParam String password, HttpServletResponse res) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email, password));

            UserDetails userDetails = jpaUserDetailsService.loadUserByUsername(email);
            Set<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority).collect(Collectors.toSet());

            if(!roles.contains("ROLE_ADMIN")){
                return "redirect:./login-error";
            }

            String jwtToken = jwtUtil.generateToken(userDetails);
            Cookie cookie = new Cookie("token",jwtToken);
            cookie.setPath("/");
            cookie.setMaxAge(420000);
            res.addCookie(cookie);
            return "redirect:./dashboard";
        }catch (Exception e){
            e.printStackTrace();
            return "redirect:./login-error";
        }


    }
    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "auth-login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletResponse res) {
        Cookie cookie = new Cookie("token",null);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        res.addCookie(cookie);

        return "redirect:./login";
    }

    @GetMapping("/order")
    public String orderAll(Model model,@RequestParam("page") Optional<Integer> page){
        int pageSize =10;
        int pageNo   =1;
        if(page.isPresent())
            pageNo=page.get();

        List<OrderResponse> listOrder = orderService.getOrderListAdmin(pageSize,pageNo,"");
        int countAll = orderService.getOrderListAdmin(Integer.MAX_VALUE,1,"").size();

        int totalPage = countAll/pageSize;
        if(totalPage*pageSize<countAll){
            totalPage +=1;
        }

        model.addAttribute("orders",listOrder);
        model.addAttribute("total",totalPage==0?1:totalPage);
        model.addAttribute("now",pageNo);

        return "layouts/order";
    }
    @GetMapping("/order-confirm")
    public String orderAllConfirm(Model model,@RequestParam("page") Optional<Integer> page){
        int pageSize =10;
        int pageNo   =1;
        if(page.isPresent())
            pageNo=page.get();

        List<AdminOrderConfirmResponse> listOrder = orderService.getOrderListConfirm(pageSize,pageNo);
        int countAll = orderService.getOrderListConfirm(Integer.MAX_VALUE,1).size();

        int totalPage = countAll/pageSize;
        if(totalPage*pageSize<countAll){
            totalPage +=1;
        }

        model.addAttribute("orders",listOrder);
        model.addAttribute("total",totalPage==0?1:totalPage);
        model.addAttribute("now",pageNo);

        return "layouts/order-confirm";
    }
    @GetMapping("/order-packing")
    public String orderAllPacking(Model model,@RequestParam("page") Optional<Integer> page){
        int pageSize =10;
        int pageNo   =1;
        if(page.isPresent())
            pageNo=page.get();

        List<AdminOrderPackingResponse> listOrder = orderService.getListPacking(pageNo,pageSize);
        int countAll = orderService.getListPacking(1,Integer.MAX_VALUE).size();

        int totalPage = countAll/pageSize;
        if(totalPage*pageSize<countAll){
            totalPage +=1;
        }

        model.addAttribute("orders",listOrder);
        model.addAttribute("total",totalPage==0?1:totalPage);
        model.addAttribute("now",pageNo);

        return "layouts/order-packing";
    }



    @GetMapping("/dashboard")
    public String dashboard(Model model){
        int user = userRepository.findAll().size();
        int product = productRepository.findAll().size();
        int order = orderRepository.findAll().size();
        Pageable pageable= PageRequest.of(0,Integer.MAX_VALUE);
        List<Payment> paymentList= paymentRepository.findAllByStatus(pageable, Payment.SELESAI);
        Integer revenue = paymentList.stream()
                .map(Payment::getTotalBill).mapToInt(Integer::intValue).sum();

        model.addAttribute("user",user);
        model.addAttribute("product",product);
        model.addAttribute("order",order);
        model.addAttribute("revenue", CommonFunction.curenyFormat(revenue.doubleValue()));
        return "layouts/dashboard";
    }

    @GetMapping("/order/detail/{id}")
    public String getDetail(@PathVariable Long id,Model model) throws BadRequestException {
        AdminOrderDetail order = orderService.getDetailAdmin(id);
        String returnVal = "layouts/detail-order";
        if(order.getStatusDetail().contains("diperiksa")){
            returnVal = "layouts/detail-confirm";
        }
        if(order.getStatusDetail().contains("dikemas")){
            returnVal="layouts/detail-packing";
        }

        model.addAttribute("order",order);
        return returnVal;
    }

    @PostMapping("/order/confirm/{id}")
    public String updatePayment(@PathVariable Long id,@RequestParam String status) throws BadRequestException {

        if(status.equalsIgnoreCase("berhasil")){
            orderService.updatePayment(id,1);
        }
        else if(status.equalsIgnoreCase("gagal")){
            orderService.updatePayment(id,0);
        }
        else return "redirect:../detail/"+id;

        return "redirect:../../order-confirm";
    }
    @PostMapping("/order/packing/{id}")
    public String updatePacking(@PathVariable Long id,@RequestParam String resi) throws BadRequestException {
        orderService.updatePacking(id,resi);
        return "redirect:../../order-packing";
    }



}
