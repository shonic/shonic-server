package com.example.shonicserver.controller;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.payload.request.CheckCost;
import com.example.shonicserver.payload.request.CheckoutRequest;
import com.example.shonicserver.service.CheckoutService;
import com.example.shonicserver.service.OrderService;
import com.example.shonicserver.service.RajaongkirService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/checkout")
@Api(tags = "Checkout")
@CrossOrigin("*")
public class CheckoutController {

    @Autowired
    private CheckoutService checkoutService;
    @Autowired
    private RajaongkirService rajaongkirService;
    @Autowired
    private OrderService orderService;
    @PostMapping("/cost")
    public ResponseEntity<Response> getAllCourierAndCost(@RequestBody CheckCost data) throws BadRequestException {
        List<Object> result = checkoutService.getConstAllCourier(data,getEmailLogin());
        if(result.size()>1)
            return new ResponseEntity<>(new Response(200,"Success",result,null), HttpStatus.OK);
        else
            return new ResponseEntity<>(new Response(400, (String) result.get(0),null,null), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/create")
    public ResponseEntity<Response> createCheckout(@RequestBody CheckoutRequest data){
        String message = orderService.createOrder(data,getEmailLogin());
        if(message == null)
            return new ResponseEntity<>(new Response(500,"Failed Checkout",null,null), HttpStatus.INTERNAL_SERVER_ERROR);
        else if(message.contains("Success")){
            message = message.split(" ")[1];
            Map<String,String> result = new HashMap<>();
            result.put("order_id",message);
            return new ResponseEntity<>(new Response(201,"Success",result,null), HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(new Response(400,message,null,"Failed"), HttpStatus.BAD_REQUEST);
        }

    }
    private String getEmailLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }
}
