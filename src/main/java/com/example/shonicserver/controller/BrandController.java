package com.example.shonicserver.controller;

import com.example.shonicserver.dto.BrandHomepage;
import com.example.shonicserver.model.Brand;
import com.example.shonicserver.payload.Response;
import com.example.shonicserver.repository.BrandRepository;
import com.example.shonicserver.service.BrandService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/v1/brand")
@CrossOrigin(origins = "*")
@Api(tags = "Brand")
public class BrandController {
    @Autowired
    BrandService brandServie;

    @Autowired
    private BrandRepository brandRepository;

    @GetMapping("/getAllBrand")
    public ResponseEntity<Response> getAllBrand(){
        List<Brand> brandList = brandServie.getListBrand();
        return new ResponseEntity<>(new Response(200,"success",brandList,null), HttpStatus.OK);
    }

    @GetMapping("/homepage")
    public ResponseEntity<Response> getBrandsHomepage(){

        return new ResponseEntity<>(new Response(200,"success",getBrandHomeList(),null), HttpStatus.OK);
    }

    @GetMapping("/getImage")
    public ResponseEntity<Response> getImageBrand(@RequestParam("name_brand")String name){
        List<BrandHomepage> brandList = getBrandHomeList();
        for (BrandHomepage b:brandList
             ) {
            if(b.getName().toLowerCase(Locale.ROOT).equals(name.toLowerCase())){
                String url = b.getImageUrl().replace("h_200","h_500");
                return new ResponseEntity<>(new Response(200,"success",url,null), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(new Response(400,"failed",null,null), HttpStatus.BAD_REQUEST);
    }

    private List<BrandHomepage>getBrandHomeList(){
        List<BrandHomepage> results = new ArrayList<>();
        results.add(new BrandHomepage("Apple","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214398/brand/Frame_10_vnxh1f.png"));
        results.add(new BrandHomepage("ASUS","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214398/brand/Frame_11_glttfj.png"));
        results.add(new BrandHomepage("Samsung","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214400/brand/Frame_22_yahokn.png"));
        results.add(new BrandHomepage("Xiaomi","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214402/brand/Frame_25_os7mrt.png"));
        results.add(new BrandHomepage("Acer","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214407/brand/Frame_31_my65bn.png"));
        results.add(new BrandHomepage("HP","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214412/brand/Frame_33_xwjamt.png"));
        results.add(new BrandHomepage("Logitech","https://res.cloudinary.com/shonic/image/upload/h_200/v1657214418/brand/Frame_9_moqqyn.png"));
        return results;
    }

}
