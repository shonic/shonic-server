package com.example.shonicserver.controller;

import com.example.shonicserver.payload.Response;
import com.example.shonicserver.service.FlashsaleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/flashsale")
@Api(tags = "Flashsale")
@CrossOrigin("*")
public class FlashsaleController {
    @Autowired
    private FlashsaleService flashsaleService;

    @GetMapping("/now")
    public ResponseEntity<Response> getFlashsale(@RequestParam(value = "sort",required = false,defaultValue = "")String sort){
        return new ResponseEntity<>(new Response(200,"Success",flashsaleService.getNowFlashsale(sort),null), HttpStatus.OK);
    }

}
