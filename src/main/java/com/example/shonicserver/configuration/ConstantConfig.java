package com.example.shonicserver.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@PropertySource("classpath:application.properties")
@Getter
public class ConstantConfig {
    private static ConstantConfig instance = null;
    public static final String defaultCurrency = "IDR";

    @Value("${app.base.url}")
    private String baseUrl;
    @Value("${app.images.path}")
    private String imagePath;
//    @Value("${app.images.url}")
//    private String imageUrl;
//    @Value("${app.images.file.url}")
//    private String imageFileUrl;
//    @Value("${app.web.url}")
//    private String urlWeb;


    @Bean
    public static ConstantConfig getInstance() {
        if (instance == null) {
            instance = new ConstantConfig();
        }

        return instance;
    }

}

