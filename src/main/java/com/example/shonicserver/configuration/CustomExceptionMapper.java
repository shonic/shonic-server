package com.example.shonicserver.configuration;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.payload.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class CustomExceptionMapper {

//    @ExceptionHandler(value = Exception.class)
//    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
//    public ResponseEntity<Response> forbidden(HttpServletRequest request, Exception e) {
//        log.error("ERROR", e);
//        return new ResponseEntity<>(new Response(500,"Internal Server Error",null,null), HttpStatus.INTERNAL_SERVER_ERROR);
//    }

    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<Response> accessDenied(HttpServletRequest request, AccessDeniedException e) {
        return new ResponseEntity<>(new Response(401,"Unauthorized access",null,null), HttpStatus.UNAUTHORIZED);
    }

//    @ExceptionHandler(value = UnathorizedException.class)
//    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
//    public BaseResponse<Object> unauthorized(HttpServletRequest request, UnathorizedException e) {
//        return BaseResponse.unauthorized();
//    }

    @ExceptionHandler(value = BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<Response>  badRequest(HttpServletRequest request, BadRequestException e) {
        return new ResponseEntity<>(new Response(400,e.getMessage(),null,null), HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler(value = NoHandlerFoundException.class)
//    @ResponseStatus(value = HttpStatus.NOT_FOUND)
//    public BaseResponse<Object> notFound(HttpServletRequest request, NoHandlerFoundException e) {
//        return BaseResponse.failed("Not Found Path");
//    }
//
//    @ExceptionHandler(value = NotFoundException.class)
//    @ResponseStatus(value = HttpStatus.NOT_FOUND)
//    public BaseResponse<Object> notFound(HttpServletRequest request, NotFoundException e) {
//        return BaseResponse.failed(e.getMessage());
//    }
//
//    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
//    public BaseResponse<Object> badRequest(HttpServletRequest request, MaxUploadSizeExceededException e) {
//        return BaseResponse.failed("File too large!");
//    }
}
