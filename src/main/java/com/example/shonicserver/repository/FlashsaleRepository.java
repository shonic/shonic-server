package com.example.shonicserver.repository;

import com.example.shonicserver.model.FlashSale;
import com.example.shonicserver.model.Order;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface FlashsaleRepository extends JpaRepository<FlashSale,Long> {

    Optional<FlashSale> findByStartTime(LocalDateTime startTime);

    Optional<FlashSale> findTopByOrderByIdDesc();

    Optional<FlashSale> findByStartTimeLessThanEqualAndEndTimeGreaterThan(LocalDateTime startTime,LocalDateTime endTime);

    Optional<FlashSale> findBySequence(Integer seq);
}
