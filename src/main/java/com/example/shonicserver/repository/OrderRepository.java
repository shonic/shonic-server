package com.example.shonicserver.repository;

import com.example.shonicserver.model.Order;
import com.example.shonicserver.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    Order findTopByOrderByIdDesc();

    List<Order> findAllByUser(Pageable pageable, User user);


    @Query("SELECT o from Order o where o.user = :user and o.status IN :status")
    List<Order> getAllByUserAndStatus(Pageable pageable,@Param("user") User user,List<String> status);

    @Query("SELECT o from Order o where o.status IN :status")
    List<Order> getAllByStatus(Pageable pageable,List<String> status);

    @Query("SELECT o from Order o ")
    List<Order> getAll(Pageable pageable);

    @Query("SELECT o FROM Order o JOIN o.payment p WHERE o.status = :orderStatus AND p.status = :paymentStatus")
    List<Order> getByOrderStatusAndPaymentStatus(Pageable pageable,@Param("orderStatus")String order,@Param("paymentStatus")String payment);

}
