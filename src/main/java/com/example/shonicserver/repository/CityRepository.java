package com.example.shonicserver.repository;

import com.example.shonicserver.model.City;
import com.example.shonicserver.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City,Long> {


    List<City> findAllByProvince(Province province);
}
