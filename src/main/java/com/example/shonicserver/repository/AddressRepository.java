package com.example.shonicserver.repository;

import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Addresses,Long> {

    Optional<Addresses> findByUser(User user);
}
