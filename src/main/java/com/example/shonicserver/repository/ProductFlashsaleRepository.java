package com.example.shonicserver.repository;

import com.example.shonicserver.model.FlashSale;
import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.ProductFlashsale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductFlashsaleRepository extends JpaRepository<ProductFlashsale,Long> {

    Optional<ProductFlashsale> findByProductAndFlashsale(Product product, FlashSale flashSale);

    List<ProductFlashsale> findByFlashsale(FlashSale flashSale);
}
