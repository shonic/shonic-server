package com.example.shonicserver.repository;

import com.example.shonicserver.model.Payment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment,Long> {

    List<Payment> findByPayBeforeLessThanEqualAndStatus(LocalDateTime date,String status);

    List<Payment> findAllByStatus(Pageable pageable, String status);
}
