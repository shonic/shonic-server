package com.example.shonicserver.repository;

import com.example.shonicserver.model.TempFlashsale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TempFlashsaleRepository extends JpaRepository<TempFlashsale,Long> {

    List<TempFlashsale> findByFlashsaleId(Long flashsaleId);

    List<TempFlashsale> findByFlashsaleIdAndIsDeletedFalse(Long flashsaleId);
}
