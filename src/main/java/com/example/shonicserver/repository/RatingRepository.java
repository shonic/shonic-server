package com.example.shonicserver.repository;

import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.Rating;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating,Long> {
    List<Rating> findByProductAndRating(Pageable pageable, Product product,Integer rating);
    List<Rating> findByProduct(Pageable pageable, Product product);
}
