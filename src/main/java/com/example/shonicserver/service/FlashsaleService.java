package com.example.shonicserver.service;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.model.FlashSale;
import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.ProductFlashsale;
import com.example.shonicserver.model.TempFlashsale;
import com.example.shonicserver.payload.request.FlashsaleAddRequest;
import com.example.shonicserver.payload.request.ProductFlashsaleRequest;
import com.example.shonicserver.payload.response.FlashsaleResponse;
import com.example.shonicserver.repository.FlashsaleRepository;
import com.example.shonicserver.repository.ProductFlashsaleRepository;
import com.example.shonicserver.repository.ProductRepository;
import com.example.shonicserver.repository.TempFlashsaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FlashsaleService {
    @Autowired
    private FlashsaleRepository flashsaleRepository;
    @Autowired
    private TempFlashsaleRepository tempFlashsaleRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductFlashsaleRepository productFlashsaleRepository;

    public void addFlashsale(FlashsaleAddRequest data) throws BadRequestException {

        String hours ;
        Integer periode = data.getPeriode();
        if(periode==1)
            hours="00:00";
        else if (periode==2)
            hours="06:00";
        else if(periode==3)
            hours="12:00";
        else if(periode==4)
            hours="18:00";
        else {
            throw new BadRequestException("Periode Not Valid");
        }

        String date = data.getDate();
        String newDate = date+" "+hours;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        LocalDateTime startTime = LocalDateTime.parse(newDate, formatter);
        LocalDateTime endTime = startTime.plusHours(6).minusSeconds(1);
        Optional<FlashSale> optionalFlashSale = flashsaleRepository.findByStartTime(startTime);
        FlashSale flashSale;
        if(optionalFlashSale.isPresent())
            flashSale =optionalFlashSale.get();
        else {
            FlashSale newFlashsale = new FlashSale();
            newFlashsale.setStartTime(startTime);
            newFlashsale.setEndTime(endTime);
            Optional<FlashSale> lastFlashsale = flashsaleRepository.findTopByOrderByIdDesc();
            int seq;
            if(lastFlashsale.isPresent())
                seq = lastFlashsale.get().getSequence();
            else seq = 0;
            seq+=1;
            newFlashsale.setSequence(seq);
            flashSale = flashsaleRepository.save(newFlashsale);
        }

        for (ProductFlashsaleRequest p:data.getProducts()
             ) {
            Optional<Product> productOptional = productRepository.findById(p.getProducId());
            if(!productOptional.isPresent())
                throw new BadRequestException("Product ID Not Found :"+p.getProducId());

            Product product = productOptional.get();
            if(product.getQty()< p.getQty())
                throw new BadRequestException("Stock Product "+p.getProducId()+" tidak cukup, Stock "+product.getQty());

            Optional<ProductFlashsale> optionalProductFlashsale = productFlashsaleRepository.findByProductAndFlashsale(product,flashSale);
            ProductFlashsale productFlashsale;
            productFlashsale = optionalProductFlashsale.orElseGet(ProductFlashsale::new);

            productFlashsale.setProduct(product);
            productFlashsale.setFlashsale(flashSale);
            productFlashsale.setDiscount(p.getDiscount());
            productFlashsale.setQty(p.getQty());
            productFlashsaleRepository.save(productFlashsale);

        }

    }

    @Scheduled(cron = "0 0-30 0,6,12,18 * * ?")
//    @Scheduled(fixedRate = 60000)
    private void jobFlashsale(){
        System.out.println("Update Flashsale");
        LocalDateTime now =LocalDateTime.now();
        Optional<FlashSale> optionalFlashSale = flashsaleRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(now,now);

        if(!optionalFlashSale.isPresent())
            return;


        FlashSale flashSale = optionalFlashSale.get();
        List<TempFlashsale>tempFlashsale = tempFlashsaleRepository.findByFlashsaleId(flashSale.getId());
        if(!tempFlashsale.isEmpty()){
            System.out.println("Flashsale is updated in temp");
            return;
        }

        List<ProductFlashsale> productFlashsales  = productFlashsaleRepository.findByFlashsale(flashSale);
        for (ProductFlashsale p:productFlashsales
             ) {
            Product product = p.getProduct();
            TempFlashsale temp = new TempFlashsale();
            temp.setFlashsaleId(flashSale.getId());
            temp.setDiscount(product.getDiscount());
            temp.setQty(product.getQty()-p.getQty());
            temp.setProductId(product.getId());
            tempFlashsaleRepository.save(temp);

            product.setQty(p.getQty());
            product.setDiscount(p.getDiscount());
            productRepository.save(product);

            System.out.println("Success Temp Product");
        }
        LocalDateTime beforeNow = LocalDateTime.now().minusHours(6);
        Optional<FlashSale> beforeFs = flashsaleRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(beforeNow,beforeNow);
        if(!beforeFs.isPresent()){
            System.out.println("before flashsale no found");
            return;
        }

        FlashSale oldFlashsale= beforeFs.get();
        List<TempFlashsale> oldTempList= tempFlashsaleRepository.findByFlashsaleId(oldFlashsale.getId());
        System.out.println("BefFlashsale start : "+oldFlashsale.getStartTime());
        if(oldTempList.isEmpty()){
            System.out.println("List Temp not found");
            return;
        }


        for (TempFlashsale t:oldTempList
             ) {
            UUID productid = t.getProductId();
            System.out.println(productid);

            Optional<Product> productOptional = productRepository.findById(productid);
            if(!productOptional.isPresent()){
                System.out.println("Product Not Found");
                return;
            }
            Product product = productOptional.get();
            System.out.println(product.getName());
            product.setDiscount(t.getDiscount());
            product.setQty(product.getQty()+t.getQty());
            productRepository.save(product);
            System.out.println("Restore temp "+product.getName());
        }

    }

    public FlashsaleResponse getNowFlashsale(String sort){

        LocalDateTime now = LocalDateTime.now();
        Optional<FlashSale> flashSaleOptional = flashsaleRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(now,now);
        if(!flashSaleOptional.isPresent())
            return null;
        List<ProductFlashsale> productFlashsales = flashSaleOptional.get().getProductFlashsale();
        if(sort.equalsIgnoreCase("termahal"))
        productFlashsales = productFlashsales.stream().sorted(Comparator.comparing(e->e.getProduct().getPrice(),Comparator.reverseOrder())).collect(Collectors.toList());
        else if (sort.equalsIgnoreCase("termurah"))
            productFlashsales = productFlashsales.stream().sorted(Comparator.comparing(e->e.getProduct().getPrice())).collect(Collectors.toList());
        else if(sort.equalsIgnoreCase("terbaru"))
            productFlashsales = productFlashsales.stream().sorted(Comparator.comparing(e->e.getProduct().getCreatedAt(),Comparator.reverseOrder())).collect(Collectors.toList());

        return FlashsaleResponse.of(productFlashsales);
    }

}

