package com.example.shonicserver.service;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.request.CheckCost;
import com.example.shonicserver.payload.request.CostApiRequest;
import com.example.shonicserver.payload.request.ProductQty;
import com.example.shonicserver.repository.ProductRepository;
import com.example.shonicserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CheckoutService {
    @Autowired
    private RajaongkirService rajaongkirService;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    public List<Object> getConstAllCourier(CheckCost data, String email) throws BadRequestException {
        User user = userRepository.getUserByUsername(email);
        Addresses address = user.getAddresses();
        if(address==null)
            throw new BadRequestException("Address Not Found");
        List<Object> result = new ArrayList<>();
        int sumWeight = 0;
        for (ProductQty p : data.getProducts()
                ) {
            Optional<Product> product = productRepository.findById(p.getProductId());
            if(!product.isPresent()){
                result.add("Product Not Found");
                return result;
            }
            sumWeight+= (int) (p.getQty()*product.get().getWeight());
            if(sumWeight>30000){
                int maxQty = (int) (30000   /product.get().getWeight());
                result.add("Product terlalu berat, jumlah maksimal "+maxQty);
                return result;
            }
        }

        CostApiRequest costApiRequest = new CostApiRequest();
        costApiRequest.setOrigin("455");
        costApiRequest.setDestination(String.valueOf(address.getCity().getId()));
        costApiRequest.setWeight(sumWeight);
        try {
            //jne
            costApiRequest.setCourier("jne");
            result.add(rajaongkirService.getCost(costApiRequest).get(0));
            //tiki
            costApiRequest.setCourier("tiki");
            result.add(rajaongkirService.getCost(costApiRequest).get(0));
//            //pos
            costApiRequest.setCourier("pos");
            result.add(rajaongkirService.getCost(costApiRequest).get(0));
            return result;
        }catch (Exception e){
            e.printStackTrace();
            throw new BadRequestException("Error Rajaongkir get Yourdata");
        }

    }



}
