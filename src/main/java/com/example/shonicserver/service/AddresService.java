package com.example.shonicserver.service;

import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.City;
import com.example.shonicserver.model.Province;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.request.AddressRequest;
import com.example.shonicserver.payload.response.AddressResponse;
import com.example.shonicserver.payload.response.CityResponse;
import com.example.shonicserver.payload.response.ProvinceResponse;
import com.example.shonicserver.repository.AddressRepository;
import com.example.shonicserver.repository.CityRepository;
import com.example.shonicserver.repository.ProvinceRepository;
import com.example.shonicserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddresService {
    @Autowired
    private RajaongkirService rajaongkirService;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    public Boolean migrateDataCity(){
        try {
            List<ProvinceResponse> provinceList = rajaongkirService.getProvinces();
            List<CityResponse> cityList =rajaongkirService.getCities();

            for (ProvinceResponse p:provinceList
            ) {
                Province province = new Province();
                province.setId(p.getId());
                province.setName(p.getName());
                provinceRepository.save(province);
            }

            for (CityResponse c:cityList
            ) {
                City city =new City();
                city.setId(c.getId());
                city.setName(c.getName());
//                city.setPostalCode(c.getPostalCode());
                Province province = provinceRepository.findById(c.getProvinceId()).get();
                city.setProvince(province);
                cityRepository.save(city);
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public List<Province> getAllProvince(){
        return provinceRepository.findAll();
    }

    public List<City> getAllCityByProvince(Long id){
        Optional<Province> provinceOptional = provinceRepository.findById(id);
        return provinceOptional.map(province -> cityRepository.findAllByProvince(province)).orElse(null);
    }

    public List<City> getAllCity(){
        return cityRepository.findAll();
    }

    public AddressResponse saveAddress(AddressRequest data, String email){
        try {
            User user = userRepository.getUserByUsername(email);
            Optional<Addresses> optionalAddresses = addressRepository.findByUser(user);

            if(optionalAddresses.isPresent()){
                Addresses address = optionalAddresses.get();
                address.setName(data.getName());
                address.setPhone(data.getPhone());
                Province province = provinceRepository.getById(data.getProvinceId());
                address.setProvince(province);
                City city = cityRepository.getById(data.getCityId());
                address.setCity(city);
                address.setKecamatan(data.getKecamatan());
                address.setDetail(data.getDetail());
                address.setPostalCode(data.getPostalCode());
                return AddressResponse.of(addressRepository.save(address));
            }else {
                Addresses address = new Addresses();
                address.setName(data.getName());
                address.setPhone(data.getPhone());
                Province province = provinceRepository.getById(data.getProvinceId());
                address.setProvince(province);
                City city = cityRepository.getById(data.getCityId());
                address.setCity(city);
                address.setKecamatan(data.getKecamatan());
                address.setDetail(data.getDetail());
                address.setPostalCode(data.getPostalCode());
                address.setUser(user);
                Addresses newAddress= addressRepository.save(address);
                return AddressResponse.of(newAddress);
            }


        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Addresses getAddress(String email){
        Optional<User> userOptional = userRepository.findByUsername(email);
        if(!userOptional.isPresent())
            return null;
        Optional<Addresses> addresses = addressRepository.findByUser(userOptional.get());
        return addresses.orElse(null);
    }
}
