package com.example.shonicserver.service;

import com.example.shonicserver.dto.AccountDto;
import com.example.shonicserver.dto.ProfileDto;
import com.example.shonicserver.dto.RegisterDto;
import com.example.shonicserver.model.Account;
import com.example.shonicserver.model.Profile;
import com.example.shonicserver.model.User;
import com.example.shonicserver.payload.response.UserResponse;

import java.util.List;
import java.util.Optional;

public interface UserService {
   public UserResponse create(RegisterDto registerDto) throws Exception;
   public Optional<User> findByEmail(String email);
   public void makeAsAdmin(User user);


   public User deletedUser(String username);

    Account createAccount(AccountDto accountDto);

    List<Account> getAllAccount();


   AccountDto delete(Integer id);

    Account updateAccount(AccountDto accountDto);

    Account getById(Integer id);

    ProfileDto updateProfile(ProfileDto profileDto,String email);

    ProfileDto getProfileByUsername(String username);


    //Boolean delete();
}
