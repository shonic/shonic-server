package com.example.shonicserver.service;

import com.example.shonicserver.exception.BadRequestException;
import com.example.shonicserver.model.*;
import com.example.shonicserver.payload.request.CheckoutRequest;
import com.example.shonicserver.payload.request.CostApiRequest;
import com.example.shonicserver.payload.request.ReviewRequest;
import com.example.shonicserver.payload.response.*;
import com.example.shonicserver.repository.*;
import com.example.shonicserver.util.CommonFunction;
import com.example.shonicserver.util.PhotoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private RajaongkirService rajaongkirService;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RatingRepository ratingRepository;


    public String createOrder(CheckoutRequest data,String email){
        Optional<Product> productOptional = productRepository.findById(data.getProducts().getProductId());
        if(!productOptional.isPresent())
            return "Product Not Found";
        Product product = productOptional.get();
        if(product.getQty()<data.getProducts().getQty())
            return "Product Out of Stock";

        int sumWeight = (int) (product.getWeight() * data.getProducts().getQty());
        if(sumWeight>30000){
            int maxQty = (int) (30000/ product.getWeight());
            return "Product terlalu berat, jumlah maksimal "+maxQty;
        }

        User user = userRepository.getUserByUsername(email);
        Addresses address = user.getAddresses();
        if(address==null)
            return "Address Not Found";

        Optional<Account> accountOptional = accountRepository.findById(data.getPaymentAccountId());
        if(!accountOptional.isPresent())
            return "Payment Account ID Not Valid";
        Account account = accountOptional.get();

        List<String> courierList = Arrays.asList("jne","pos","tiki");
        if(!courierList.contains(data.getCourier().getCode().toLowerCase(Locale.ROOT)))
            return "Courier Not Valid";

        List<CourierApiResponse> courierApiResponses =new ArrayList<>();
        CostApiRequest costApiRequest = new CostApiRequest();
        costApiRequest.setOrigin("455");
        costApiRequest.setDestination(String.valueOf(address.getCity().getId()));
        costApiRequest.setWeight(sumWeight);
        try {
            costApiRequest.setCourier(data.getCourier().getCode());
            courierApiResponses =  rajaongkirService.getCost(costApiRequest);
        }catch (Exception e){
            e.printStackTrace();
            return "Error Get Rajaongkir Data";
        }
        CourierApiResponse courier = courierApiResponses.get(0);
        ServiceApiResponse  courierService = null;
        for (ServiceApiResponse s : courier.getCosts()
                ) {
            if(s.getService().equalsIgnoreCase(data.getCourier().getService())){
                courierService = s;
            }
        }
        if(courierService == null)
            return "Courier Service Not Supported";
        int discountAmount = product.getDiscount() > 0 ? product.getPrice()*product.getDiscount()/100:0;

        int subTotal= (product.getPrice()-discountAmount) * data.getProducts().getQty();
        int cost = courierService.getCost().get(0).getValue();
        String etd = courierService.getCost().get(0).getEtd();
        int total = subTotal+cost;

        Addresses orderAddress = addressRepository.save(Addresses.of(address));
        Payment payment = Payment.builder()
                .account(account)
                .totalBill(total)
                .status(Payment.BELUM_BAYAR).build();

        payment = paymentRepository.save(payment);

        Order order = Order.builder()
                .product(product)
                .qty(data.getProducts().getQty())
                .discount(product.getDiscount())
                .payment(payment)
                .address(orderAddress)
                .cost(cost)
                .courierCode(courier.getCode())
                .courierName(courier.getName())
                .courierService(courierService.getService())
                .courierEtd(etd)
                .subTotal(subTotal)
                .total(total)
                .orderNumber(generateSOCode())
                .status(Order.BELUM_BAYAR)
                .note(data.getNote())
                .user(user)
                .build();
        Long idOrder = orderRepository.save(order).getId();
        product.setQty(product.getQty()-data.getProducts().getQty());
        productRepository.save(product);
        return "Success "+idOrder;
    }



    public PaymentResponse getPayment(Long id,String email) throws BadRequestException {
        User user = userRepository.getUserByUsername(email);
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            throw new BadRequestException("Order Not Found");

        Order order = orderOptional.get();
        if(!order.getUser().getId().equals(user.getId())){
            throw new BadRequestException("Access Denied");
        }
        if(!order.getPayment().getStatus().equals(Payment.BELUM_BAYAR)){
            throw new BadRequestException("Pembayaran tidak tersedia, pesanan telah "+order.getStatus());
        }
        Account payAccount = order.getPayment().getAccount();

        return PaymentResponse.builder()
                .orderId(order.getId())
                .bank(payAccount.getBank())
                .name(payAccount.getName())
                .branch(payAccount.getBranch())
                .image(payAccount.getImage())
                .number(payAccount.getNumber())
                .payBefore(CommonFunction.getPayBefore(order.getPayment().getPayBefore()))
                .totalPayment(order.getPayment().getTotalBill())
                .build();



    }

    public String savePayment(Long id,String name,String bankName,String number,Integer total,MultipartFile file,String email) throws BadRequestException, IOException {
        if(file==null)
            throw new BadRequestException("Photo is Required");
        validateImage(file);

        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            throw new BadRequestException("Order Not Found");
        Order order=orderOptional.get();
        if(!order.getUser().getUsername().equals(email)){
            throw new BadRequestException("Access Denied");
        }
        if(!order.getPayment().getStatus().equals(Payment.BELUM_BAYAR)){
            throw new BadRequestException("Pembayaran tidak tersedia, pesanan telah "+order.getStatus());
        }
        Payment payment = order.getPayment();
        String fileNameFormat = "payment_" + CommonFunction.generateRandom(order.getId().toString());
        payment.setImageUrl(PhotoUtil.uploadImagePayment(file, "payment", fileNameFormat));
        payment.setName(name);
        payment.setBankNumber(number);
        payment.setTotalTransfer(total);
        payment.setBankName(bankName);
        payment.setPayBefore(null);
        payment.setStatus(Payment.DIBAYAR);
        paymentRepository.save(payment);
        order.setStatus(Order.DIPROSES);
        orderRepository.save(order);
        return "Success";

    }

    public void receivedOrder(Long id,String email) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            throw new BadRequestException("Pesanan tidak ditmukan");
        Order order = orderOptional.get();
        if(!order.getUser().getUsername().equals(email))
            throw new BadRequestException("Access Denied");

        if(order.getStatus().equals(Order.SELESAI))
            throw new BadRequestException("Pesanan telah diterima");

        if(!order.getStatus().equals(Order.DIKIRIM))
            throw new BadRequestException("Status pesanan "+order.getStatus()+",tidak dapat diselesaikan");
        order.setStatus(Order.SELESAI);
        order.setRateBefore(LocalDateTime.now().plusMonths(1));
        orderRepository.save(order);
    }

    public void saveReview(Long id, String email, ReviewRequest data) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            throw new BadRequestException("Pesanan tidak ditmukan");
        Order order = orderOptional.get();
        if(!order.getUser().getUsername().equals(email))
            throw new BadRequestException("Access Denied");

        if(!order.getStatus().equals(Order.SELESAI))
            throw new BadRequestException("Pesanan tidak dapat dinilai");
        if(order.getReviewed())
            throw new BadRequestException("Pesanan telah dinilai");

        if(data.getRating()==null)
            throw new BadRequestException("Rating is Required");
        Product product = order.getProduct();

        Rating newRating = new Rating();
        newRating.setRating(data.getRating());
        newRating.setUlasan(data.getReview());
        newRating.setProduct(product);
        newRating.setUser(order.getUser());
        ratingRepository.save(newRating);

        float rating = product.getRating();
        int reviewCount = product.getReview();
        float sumRating = rating * reviewCount;
        reviewCount+=1;
        sumRating += data.getRating();
        float totalRating = sumRating/reviewCount;
        DecimalFormat df = new DecimalFormat("#.##");
        product.setRating(Float.valueOf(df.format(totalRating)));
        product.setReview(reviewCount);
        productRepository.save(product);
        order.setReviewed(true);
        order.setRateBefore(null);
        orderRepository.save(order);

    }

    private String generateSOCode() {
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("MMdd");
        String datetime = ft.format(dNow);
        Order order = orderRepository.findTopByOrderByIdDesc();
        Long id;
        if(order==null)
            id=1L;
        else id = order.getId();
        String numberOrder =  String.format("%03d", id+1);
        return "ORD"+datetime+numberOrder;
    }

    public List<OrderResponse> getOrderList(int pageSize,int pageNo,String status,String email){

        Pageable pageable= PageRequest.of(pageNo-1,pageSize, Sort.Direction.DESC,"id");

        List<Order> orders;
        User user = userRepository.getUserByUsername(email);
        if(status.isEmpty()){
            orders = orderRepository.findAllByUser(pageable,user);
        }else{
            List<String> statusList = Arrays.asList(status.split(","));
            List<String> statuses=new ArrayList<>();
            for (String s:statusList
                 ) {
                if(s.equalsIgnoreCase(Order.BELUM_BAYAR)) statuses.add(Order.BELUM_BAYAR);
                if(s.equalsIgnoreCase(Order.DIPROSES)) statuses.add(Order.DIPROSES);
                if(s.equalsIgnoreCase(Order.DIKIRIM)) statuses.add(Order.DIKIRIM);
                if(s.equalsIgnoreCase(Order.DIBATALKAN)) statuses.add(Order.DIBATALKAN);
                if(s.equalsIgnoreCase(Order.SELESAI)) statuses.add(Order.SELESAI);
            }
            orders = orderRepository.getAllByUserAndStatus(pageable,user,statuses);
        }
        return orders.stream().map(OrderResponse::of).collect(Collectors.toList());
    }

    public OrderDetailResponse getDetail(Long id,String email) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (!orderOptional.isPresent())
            throw new BadRequestException("Order Not Found");

        Order order = orderOptional.get();
        if(!order.getUser().getUsername().equals(email))
            throw new BadRequestException("Access Denied");

        return OrderDetailResponse.of(order);
    }

    public AdminOrderDetail getDetailAdmin(Long id) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (!orderOptional.isPresent())
            throw new BadRequestException("Order Not Found");

        Order order = orderOptional.get();

        return AdminOrderDetail.of(order);
    }

    public void cancelOrder(Long id,String email) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent()){
            throw new BadRequestException("Order ID Not Found");
        }

        if(!Objects.equals(orderOptional.get().getUser().getUsername(), email)){
            throw new BadRequestException("Access Denied");
        }

        Order order = orderOptional.get();

        if(!Objects.equals(order.getStatus(), Order.BELUM_BAYAR))
            throw new BadRequestException("Pesanan tidak dapat dibatalkan");
        order.setStatus(Order.DIBATALKAN);
        order.getPayment().setStatus(Payment.BATAL);
        order.getPayment().setPayBefore(null);
        orderRepository.save(order);

        Product product = order.getProduct();
        product.setQty(product.getQty()+order.getQty());
        productRepository.save(product);
    }
    private User getUserLogin(String email){
        return userRepository.getUserByUsername(email);
    }
    @Scheduled(fixedRate = 10000)
    public void jobPaymentExpire() {

        List<Payment> paymentList = paymentRepository.findByPayBeforeLessThanEqualAndStatus(LocalDateTime.now(),Payment.BELUM_BAYAR);
        for (Payment p:paymentList
             ) {
            p.setPayBefore(null);
            p.setStatus(Payment.EXPIRED);
            paymentRepository.save(p);
            Order order = p.getOrder();
            order.setStatus(Order.DIBATALKAN);
            orderRepository.save(order);
            Product product = order.getProduct();
            product.setQty(product.getQty()+order.getQty());
            productRepository.save(product);
            System.out.println("Expired Order ID : "+order.getId());
        }
    }

    public List<AdminOrderConfirmResponse> getOrderListConfirm(int pageSize,int pageNo){

        Pageable pageable= PageRequest.of(pageNo-1,pageSize, Sort.Direction.ASC,"updatedAt");
        List<Payment> paymentList = paymentRepository.findAllByStatus(pageable,Payment.DIBAYAR);
        return paymentList.stream().map(AdminOrderConfirmResponse::of).collect(Collectors.toList());
    }

    public List<AdminOrderPackingResponse> getListPacking(int pageNo,int pageSize){
        Pageable pageable= PageRequest.of(pageNo-1,pageSize, Sort.Direction.ASC,"updatedAt");
        List<Order> orderList = orderRepository.getByOrderStatusAndPaymentStatus(pageable,Order.DIPROSES,Payment.SELESAI);
        return orderList.stream().map(AdminOrderPackingResponse::of).collect(Collectors.toList());
    }

    public void updatePacking(Long id,String resi) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            throw new BadRequestException("Order Not Found");
        if (resi.isEmpty())
            throw new BadRequestException("Resi Not Valid");

        Order order = orderOptional.get();
        if(!(order.getStatus().equals(Order.DIPROSES) && order.getPayment().getStatus().equals(Payment.SELESAI)))
            throw new BadRequestException("Can't Update Packing");

        order.setResi(resi);
        order.setStatus(Order.DIKIRIM);
        orderRepository.save(order);
    }
    private void validateImage(MultipartFile file) throws BadRequestException {
        String ext = PhotoUtil.getFileExtension(file);
        if (!(ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("jpeg"))) {
            throw new BadRequestException("Image must be PNG/JPG/JPEG");
        }

        if(file.getSize()>25000000){
            throw new BadRequestException("Image too large, max 2MB");
        }
    }

    public void updatePayment(Long id,Integer status) throws BadRequestException {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent()){
            throw new BadRequestException("Order Not Found");
        }
        Order order = orderOptional.get();
        Payment payment = order.getPayment();
        if(!payment.getStatus().equals(Payment.DIBAYAR)){
            throw new BadRequestException("Order Cannot Update");
        }
        if(!order.getStatus().equals(Order.DIPROSES)){
            throw new BadRequestException("Order Cannot Update");
        }
        if(status == 1){
            payment.setStatus(Payment.SELESAI);
            order.setStatus(Order.DIPROSES);
        }else if(status==0){
            payment.setStatus(Payment.KURANG);
            order.setStatus(Order.DIBATALKAN);
        }else {
            throw new BadRequestException("Status Not Valid");
        }
        order.setPayment(payment);
        orderRepository.saveAndFlush(order);
    }

    public List<OrderResponse> getOrderListAdmin(int pageSize,int pageNo,String status){

        Pageable pageable= PageRequest.of(pageNo-1,pageSize, Sort.Direction.DESC,"id");

        List<Order> orders;
        if(status.isEmpty()){
            orders = orderRepository.getAll(pageable);
        }else{
            List<String> statusList = Arrays.asList(status.split(","));
            List<String> statuses=new ArrayList<>();
            for (String s:statusList
            ) {
                if(s.equalsIgnoreCase(Order.BELUM_BAYAR)) statuses.add(Order.BELUM_BAYAR);
                if(s.equalsIgnoreCase(Order.DIPROSES)) statuses.add(Order.DIPROSES);
                if(s.equalsIgnoreCase(Order.DIKIRIM)) statuses.add(Order.DIKIRIM);
                if(s.equalsIgnoreCase(Order.DIBATALKAN)) statuses.add(Order.DIBATALKAN);
                if(s.equalsIgnoreCase(Order.SELESAI)) statuses.add(Order.SELESAI);
            }
            orders = orderRepository.getAllByStatus(pageable,statuses);
        }
        return orders.stream().map(OrderResponse::of).collect(Collectors.toList());
    }
}
