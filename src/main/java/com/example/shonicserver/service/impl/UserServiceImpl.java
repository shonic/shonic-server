package com.example.shonicserver.service.impl;
import com.example.shonicserver.dto.AccountDto;
import com.example.shonicserver.dto.ProfileDto;
import com.example.shonicserver.dto.RegisterDto;
import com.example.shonicserver.model.*;
import com.example.shonicserver.payload.response.UserResponse;
import com.example.shonicserver.repository.ProfileRepository;
import com.example.shonicserver.repository.AccountRepository;
import com.example.shonicserver.repository.RoleRepository;
import com.example.shonicserver.repository.UserRepository;
import com.example.shonicserver.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private AccountRepository accountRepisitory;

    @Override
    public UserResponse create(RegisterDto registerDto) throws Exception {
        ERole name = ERole.ROLE_CUSTOMER;

        Optional<Role> roleOptional = this.roleRepository.findByName(name);
        Role role;
        if(roleOptional.isPresent()){
            role = roleOptional.get();
        }
        else{
            Role newRole = new Role();
            newRole.setName(name);
            role = roleRepository.save(newRole);
        }
        Set<Role> roles = new HashSet<>();
        roles.add(role);


        User user = new User();
        user.setUsername(registerDto.getEmail());
        user.setFullname(registerDto.getFullname());
        user.setPassword(bCryptPasswordEncoder.encode(registerDto.getPassword()));
        user.setRoles(roles);


        User userSaved = this.userRepository.save(user);
            UserResponse userResponse = new UserResponse();
            userResponse.setId(userSaved.getId());
            userResponse.setEmail(userSaved.getUsername());
            userResponse.setFullName(userSaved.getFullname());
        return userResponse;
    }

    public Optional<User> findByEmail(String email){
        return userRepository.findByUsername(email);
    }

    public void makeAsAdmin(User user){
        Optional<Role> roleOptional = roleRepository.findByName(ERole.ROLE_ADMIN);
        Role roleAdmin;
        if(roleOptional.isPresent()){
            roleAdmin = roleOptional.get();
        }else{
            Role role = new Role();
            role.setName(ERole.ROLE_ADMIN);
            roleAdmin = roleRepository.save(role);
        }
        Role roleCustomer = roleRepository.findByName(ERole.ROLE_CUSTOMER).get();
        Set<Role> newRole = new HashSet<>();
        newRole.add(roleAdmin);
        newRole.add(roleCustomer);
        user.setRoles(newRole);
        userRepository.save(user);

    }

    @Override
    public User deletedUser(String email) {
      /*  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();*/
        Optional<User> userOptional= userRepository.findByUsername(email);
        if(userOptional!=null){
            User user=userOptional.get();
            user.setDeleted(true);
            return userRepository.save(user);
        }
        return null;
    }

    @Override
    public Account createAccount(AccountDto accountDto) {
        Account account=new Account();
        BeanUtils.copyProperties(accountDto,account);
        Account save=this.accountRepisitory.save(account);
        return save;
    }

    @Override
    public List<Account> getAllAccount() {
        List<Account>accountList=this.accountRepisitory.findAllByIsDeletedFalse();
       // List<AccountDto>accountDtoList=new ArrayList<>();
      /* for (Account account:accountList){
            AccountDto dto=new AccountDto();
            dto.setName(account.getName());
            dto.setBank(account.getBank());
            dto.setBranch(account.getBranch());
            dto.setNumber(account.getNumber());
           // dto.setId(account.getId());
            dto.setIsDeleted(false);
            accountDtoList.add(dto);
        }*/
        return accountList;
    }

    @Override
    public AccountDto delete(Integer id) {
        Optional<Account> accountOptional=accountRepisitory.findById(id);
        Account account=null;
        if(accountOptional.isPresent()){
            account=accountOptional.get();
            account.setIsDeleted(true);
            Account accountUpdate=accountRepisitory.save(account);
            AccountDto accountDtoUpdate=converAccountToAccountDto(accountUpdate);
            return accountDtoUpdate;
        }
        return null;
    }
    @Override
    public ProfileDto getProfileByUsername(String username) {

        Optional<User> profileOptional=userRepository.findByUsername(username);
        if(profileOptional.isPresent()){
                Optional<Profile> optionalProfile=profileRepository.findByUser(profileOptional.get());
                if(!optionalProfile.isPresent()){
                    Profile profile=new Profile();
                    profile.setUser(profileOptional.get());
                    Profile profile1=profileRepository.save(profile);
                    ProfileDto profileDtoUpdate=convertProfileToProfileDto(profile1);
                    return profileDtoUpdate;
                }else {

                    return convertProfileToProfileDto(optionalProfile.get());
                }



        }
        return null;
    }



    @Override
    public ProfileDto updateProfile(ProfileDto profileDto,String email) {
        User userfind=userRepository.getUserByUsername(email);
        Profile profileupdate=userfind.getProfile();
        userfind.setFullName(profileDto.getNama());
        profileupdate.setNomorHp(profileDto.getNomorHp());
        profileupdate.setGender(profileDto.getGender());
        profileupdate.setBirthdate(getDateTimeStartFromDate(profileDto.getBirthdate()));

        userfind.setProfile(profileupdate);
        return convertProfileToProfileDto(this.userRepository.save(userfind).getProfile());
    }

    private  LocalDateTime getDateTimeStartFromDate(String date) {
        return getDateFrom(date.concat(" 00:00:00"), "dd/MM/yyy HH:mm:ss");
    }
    private LocalDateTime getDateFrom(String date, String format) {
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern(format));
    }
    public static String getDateTime(LocalDateTime date) {
        return getDateFrom(date, "dd/MM/yyyy");
    }
    public static String getDateFrom(LocalDateTime date, String format) {
        if (date == null) return "";
        return date.format(DateTimeFormatter.ofPattern(format));
    }

    @Override
    public Account updateAccount(AccountDto accountDto) {
        Account account =new Account();
        BeanUtils.copyProperties(accountDto,account);
        return this.accountRepisitory.save(account);
    }

    @Override
    public Account getById(Integer id) {
        Optional<Account>accountOptional=accountRepisitory.findById(id);
        Account account=new Account();
        if(accountOptional.isPresent()){
            account=accountOptional.get();
            return account;

        }
        return null;
    }

    private ProfileDto convertProfileToProfileDto(Profile profile1) {
      //  Optional<Profile>existingP=profileRepository.findById(profile1.getId());
        ProfileDto profileDtoDto=new ProfileDto();
        profileDtoDto.setNama(profile1.getUser().getFullName());
        profileDtoDto.setEmail(profile1.getUser().getUsername());
        profileDtoDto.setGender(profile1.getGender());

        profileDtoDto.setBirthdate(profile1.getBirthdate()==null?"":getDateTime(profile1.getBirthdate()));
        profileDtoDto.setNomorHp(profile1.getNomorHp());
        return profileDtoDto;
    }

    private AccountDto converAccountToAccountDto(Account accountCreated){
        Optional<Account>existingAccountOptional=accountRepisitory.findById(accountCreated.getId());
        AccountDto accountDtoCreated=new AccountDto();
        BeanUtils.copyProperties(accountCreated,accountDtoCreated);
        return accountDtoCreated;

    }


}
