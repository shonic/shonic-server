package com.example.shonicserver.service;

import com.example.shonicserver.payload.request.CostApiRequest;
import com.example.shonicserver.payload.response.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import java.util.stream.Collectors;

@Service
public class RajaongkirService {
    @Value("${rajaongkir.api.url}")
    private       String       url;
    @Value("${rajaongkir.api.key}")
    private       String       key;




    private HttpHeaders getParamHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("key", key);



        return headers;
    }


    private List<Map<String,String>> get(String path){
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity request = new HttpEntity(getParamHeader());
            ResponseEntity<Map> response = restTemplate.exchange(
                    url+path,
                    HttpMethod.GET,
                    request,
                    Map.class
            );
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString( response.getBody().get("rajaongkir"));
            RajaongkirResponse rajaongkir = mapper.readValue(json, RajaongkirResponse.class);
            return rajaongkir.getResults();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }

    }

    public List<ProvinceResponse> getProvinces(){
        List<Map<String,String>> map = get("/province");
        return map.stream().map(ProvinceResponse::of).collect(Collectors.toList());
    }

    public List<CityResponse> getCities(){
        List<Map<String,String>> map = get("/city");
        return map.stream().map(p->CityResponse.of(p)).collect(Collectors.toList());
    }

    private BaseRajaongkirResponse post(String path,Object body){
        HttpHeaders headers = getParamHeader();
        HttpEntity request = new HttpEntity<>(body,headers);
        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Object> response = restTemplate.exchange(
                    url+path,
                    HttpMethod.POST,
                    request,
                    Object.class
            );
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString( response.getBody());
            return mapper.readValue(json,BaseRajaongkirResponse.class);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<CourierApiResponse> getCost(CostApiRequest query){
        return Objects.requireNonNull(post("/cost", query)).getRajaongkir().getResults();
    }
}


