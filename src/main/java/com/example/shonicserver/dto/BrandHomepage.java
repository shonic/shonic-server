package com.example.shonicserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BrandHomepage {
    private String name;
    @JsonProperty("image_url")
    private String imageUrl;
}
