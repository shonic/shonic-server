package com.example.shonicserver.dto;

import com.example.shonicserver.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountDto {
    private String name;
    private String number;
    private String bank;
    private String branch;
    private String image;

}
