package com.example.shonicserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "addresses")
@Builder
public class Addresses implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String phone;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "city_id")
    private City city;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "province_id")
    private Province province;

    private String kecamatan;

    @JsonProperty("detail_address")
    private String detail;

    @Column(name = "postal_code")
    private String postalCode;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id", unique = true)
    private User user;

    public static Addresses of(Addresses addresses){
        return Addresses.builder()
                .city(addresses.getCity())
                .detail(addresses.getDetail())
                .kecamatan(addresses.getKecamatan())
                .name(addresses.getName())
                .phone(addresses.getPhone())
                .postalCode(addresses.getPostalCode())
                .province(addresses.getProvince())
                .build();
    }
}
