package com.example.shonicserver.model;

import com.example.shonicserver.configuration.ConstantConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


import javax.persistence.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payment")
@Builder
public class Payment extends BaseEntity{
    public static final String BELUM_BAYAR = "Belum Bayar";
    public static final String DIBAYAR = "Dibayar";
    public static final String KURANG = "Kurang";
    public static final String EXPIRED = "Expired";
    public static final String DIKEMBALIKAN = "Dikembalikan";
    public static final String SELESAI = "Selesai";
    public static final String BATAL ="Dibatalkan";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account")
    private Account account;
    @JsonProperty("bank_number")
    private String bankNumber;
    private String name;
    private String bankName;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("total_transfer")
    private Integer totalTransfer;
    @JsonProperty("total_bill")
    private Integer totalBill;
    private LocalDateTime payBefore;

    @JsonIgnore
    @OneToOne(mappedBy = "payment")
    private Order order;


    @PrePersist
    protected void prePersistPayment() {
        payBefore =LocalDateTime.now().plusHours(24);
    }


    public String getImageUrlStr(){
        String baseUrl = ConstantConfig.getInstance().getBaseUrl();
        return baseUrl+"/images/payment/"+imageUrl;
    }



}
