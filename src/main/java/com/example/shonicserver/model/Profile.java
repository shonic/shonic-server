package com.example.shonicserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "profile")
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id",referencedColumnName = "id",unique = true)
    private User user;

    @Column(name = "nomor_hp")
    private String nomorHp;

    @Column(name = "gender")
    private String gender;

    @Column(name = "BirthDate")
    private LocalDateTime birthdate;

    /*@Column(name = "photo")
    private String photo;*/

}
