package com.example.shonicserver.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "temp_flashsale")
public class TempFlashsale extends BaseEntity{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty("product_id")
    private UUID productId;
    @JsonProperty("flashsala_id")
    private Long flashsaleId;
    private Integer qty;
    private Integer discount;
}
