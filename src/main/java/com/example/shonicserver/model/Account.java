package com.example.shonicserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "account")
public class Account {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "number")
    private String number;
    @Column(name = "bank")
    private String bank;
    @Column(name = "branch")
    private String branch;
    @Column(name = "image")
    private String image;
    @JsonIgnore
    @Column(name = "is_delete")
    private Boolean isDeleted;

    @PrePersist
    protected void prePersist() {
        this.isDeleted = false;
    }

}
