package com.example.shonicserver.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends BaseEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    // username as email uniq
   @Column(name = "email")
   private String username;


    // email unique
   /* @Column(name = "email", nullable = false)
    private String email;*/

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getFullname() {
        return fullname;
    }

    public Rating getRating() {
        return rating;
    }

    // password
    @Column(name = "password")
    private String password;
    //full name
    @Column(name = "full_name")
    private String fullname;

    // addresses one to one
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Addresses addresses;
    // role
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();
    @Enumerated(EnumType.STRING)
    @Column(name = "oauth_provider")
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    // rating
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private Rating rating;
    @OneToOne(cascade = CascadeType.PERSIST,fetch = FetchType.LAZY ,mappedBy = "user")
    private Profile profile;

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Profile getProfile() {
        return profile;
    }

    public String getFullName() {
        return fullname;
    }

    public void setFullName(String fullName) {
        this.fullname = fullName;
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Addresses getAddresses() {
        return addresses;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddresses(Addresses addresses) {
        this.addresses = addresses;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

}

