package com.example.shonicserver.model;


import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.security.core.parameters.P;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "order")
public class Order extends BaseEntity{

    public static final String BELUM_BAYAR= "Belum Bayar";
    public static final String DIPROSES = "Diproses";
    public static final String DIKIRIM = "Dikirim";
    public static final String SELESAI = "Selesai";
    public static final String DIBATALKAN = "Dibatalkan";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;
    private Integer discount;
    private Integer qty;
    private Integer subTotal;
    private String status;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "addresses")
    private Addresses address;

    @JsonProperty("order_number")
    private String orderNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "payment",referencedColumnName = "id")
    private Payment payment;

    @JsonProperty("courier_code")
    private String courierCode;
    @JsonProperty("courier_name")
    private String courierName;
    @JsonProperty("courier_service")
    private String courierService;
    @JsonProperty("courier_etd")
    private String courierEtd;
    private String resi;
    private String note;
    private Integer cost;
    private Integer total;
    private Boolean reviewed;
    private LocalDateTime rateBefore;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @PrePersist
    protected void prePersistReview() {
        reviewed =false;
    }
    public String getStatusDetail(){

            if(Objects.equals(status, Order.BELUM_BAYAR )|| Payment.BELUM_BAYAR.equals(payment.getStatus())){
                return "Bayar pesanan sebelum";
            }
            if(status.equals(Order.DIPROSES) && payment.getStatus().equals(Payment.DIBAYAR)){
                return "Pembayaran sedang diperiksa oleh admin";
            }
            if(status.equals(Order.DIPROSES) && payment.getStatus().equals(Payment.SELESAI)){
                return "Pembayaran berhasil, produk sedang dikemas";
            }
            if(status.equals(Order.DIKIRIM)){
                return "Estimasi pengiriman";
            }
            if(status.equals(Order.SELESAI)&& reviewed){
                return "Pesanan telah dinilai";
            }
            if(status.equals(Order.SELESAI)){
                return "Nilai pesanan sebelum";
            }

            if(status.equals(Order.DIBATALKAN)&& payment.getStatus().equals(Payment.KURANG)){
                return "Pembatalan otomatis: jumlah transfer tidak sesuai";
            }
            if(status.equals(Order.DIBATALKAN)&& payment.getStatus().equals(Payment.EXPIRED)){
                return "Pembatalan otomatis: waktu pembayaran habis";
            }

            if(status.equals(Order.DIBATALKAN)){
                return "Pesanan telah anda batalkan";
            }
            return null;
    }

    public String getDeadline(){
        if(status.equals(Order.BELUM_BAYAR)&& payment.getStatus().equals(Payment.BELUM_BAYAR) && payment.getPayBefore().isAfter(LocalDateTime.now())){
            return CommonFunction.getDateOrder(payment.getPayBefore());
        }
        if(status.equals(Order.SELESAI)&& !reviewed){
            return CommonFunction.getBeforeRate(rateBefore);
        }
        return null;
    }

    public String getEstimasi(){
        if(status.equals(Order.DIKIRIM)){
            return courierEtd+" hari";
        }
        return null;
    }

    public String getPaymentStr(){
        return "Transfer Bank "+payment.getAccount().getBank();
    }

}
