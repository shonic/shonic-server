package com.example.shonicserver.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FlashsaleAddRequest {
    private String date;
    private Integer periode;
    private List<ProductFlashsaleRequest> products;
}
