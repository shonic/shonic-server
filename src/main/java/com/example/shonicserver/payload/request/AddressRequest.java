package com.example.shonicserver.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AddressRequest {
    private String name;
    private String phone;
    @JsonProperty("province_id")
    private Long provinceId;
    @JsonProperty("city_id")
    private Long cityId;

    private String kecamatan;
    private String detail;
    @JsonProperty("postal_code")
    private String postalCode;


}
