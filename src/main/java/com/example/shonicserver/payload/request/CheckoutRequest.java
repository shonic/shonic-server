package com.example.shonicserver.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckoutRequest {
    private ProductQty products;
    private String note;
    private CourierRequest courier;
    @JsonProperty("payment_account_id")
    private Integer paymentAccountId;

}
