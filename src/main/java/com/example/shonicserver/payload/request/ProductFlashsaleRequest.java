package com.example.shonicserver.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class ProductFlashsaleRequest {
    private UUID producId;
    private Integer qty;
    private Integer discount;
}
