package com.example.shonicserver.payload.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CostApiRequest {
    private String origin;
    private String destination;
    private Integer weight;
    private String courier;
}
