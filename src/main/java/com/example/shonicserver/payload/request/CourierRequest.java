package com.example.shonicserver.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourierRequest {
    private String code;
    private String service;
}
