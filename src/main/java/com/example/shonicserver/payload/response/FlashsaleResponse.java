package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.FlashSale;
import com.example.shonicserver.model.ProductFlashsale;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
public class FlashsaleResponse {
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("ënd_time")
    private String endTime;
    @JsonProperty("countdown")
    private String countTime;
    @JsonProperty("upto_discount")
    private Integer upToDiscount;

    private List<ProductFlashsaleResponse> products;

    public static FlashsaleResponse of(List<ProductFlashsale> data){

        ProductFlashsale pfs = data.stream().max(Comparator.comparing(ProductFlashsale::getDiscount)).get();
        FlashSale f = pfs.getFlashsale();
        long millis = ChronoUnit.MILLIS.between(LocalDateTime.now(),f.getEndTime());
        String count= String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

        return FlashsaleResponse.builder()
                .startTime(f.getStartTime().toString())
                .endTime(f.getEndTime().toString())
                .countTime(count)
                .upToDiscount(pfs.getDiscount())
                .products(data.stream().map(ProductFlashsaleResponse::of).collect(Collectors.toList()))
                .build();
    }


}
