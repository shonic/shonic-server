package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Order;
import com.example.shonicserver.model.Payment;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdminOrderPackingResponse {
    @JsonProperty("order_id")
    private Long orderId;
    @JsonProperty("order_number")
    private String orderNumber;
    @JsonProperty("total_price")
    private String totalPrice;
    private String subtotal;
    private String datePayment;
    private Integer cost;
    private String note;
    private ProductOrderResponse products;
    private AddressResponse address;

    public static AdminOrderPackingResponse of(Order order){
        return AdminOrderPackingResponse.builder()
                .orderId(order.getId())
                .totalPrice(CommonFunction.curenyFormat(order.getTotal().doubleValue()))
                .subtotal(CommonFunction.curenyFormat(order.getSubTotal().doubleValue()))
                .cost(order.getCost())
                .note(order.getNote())
                .orderNumber(order.getOrderNumber())
                .products(ProductOrderResponse.of(order))
                .address(AddressResponse.of(order.getAddress()))
                .datePayment(CommonFunction.getDateOrder(order.getPayment().getUpdatedAt()))
                .build();
    }
}
