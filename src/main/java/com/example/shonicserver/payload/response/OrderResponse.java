package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Order;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderResponse{
    private Long id;
    @JsonProperty("order_number")
    private String orderNumber;
    private String status;
    @JsonProperty("status_detail")
    private String statusDetail;
    private String estimate;
    private String deadline;
    private ProductOrderResponse product;
    @JsonProperty("total_price")
    private Integer totalPrice;
    private Integer subtotal;
    private Integer cost;
    private Boolean reviewed;
    private String dateOrder;
    private String pembayaran;

    public static OrderResponse of(Order order){
        return OrderResponse.builder()
                .id(order.getId())
                .orderNumber(order.getOrderNumber())
                .status(order.getStatus())
                .statusDetail(order.getStatusDetail())
                .deadline(order.getDeadline())
                .estimate(order.getEstimasi())
                .totalPrice(order.getTotal())
                .product(ProductOrderResponse.of(order))
                .cost(order.getCost())
                .subtotal(order.getSubTotal())
                .reviewed(order.getReviewed())
                .dateOrder(CommonFunction.getDateOrder(order.getCreatedAt()))
                .pembayaran(order.getPayment().getStatus())
                .build();
    }

}
