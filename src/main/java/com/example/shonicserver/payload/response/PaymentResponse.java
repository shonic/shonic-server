package com.example.shonicserver.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentResponse {
    @JsonProperty("order_id")
    private Long orderId;
    private String name;
    private String image;
    private String number;
    private String bank;
    private String branch;
    @JsonProperty("pay_before")
    private String payBefore;
    @JsonProperty("total_payment")
    private Integer totalPayment;
}
