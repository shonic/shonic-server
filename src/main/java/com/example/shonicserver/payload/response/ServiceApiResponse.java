package com.example.shonicserver.payload.response;

import lombok.*;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceApiResponse {
    private String service;
    private String description;
    private List<CostApiResponse> cost;

//    public static ServiceApiResponse of(Map<String,Object>data){
//        return ServiceApiResponse.builder()
//                .service(String.valueOf(data.get("service")))
//                .description(String.valueOf(data.get("description")))
//                .costs(data.get(""))
//    }
}
