package com.example.shonicserver.payload.response;


import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class RajaongkirResponse {
    private List<Object> query;
    private Object status;
    private List<Map<String,String>> results;

}
