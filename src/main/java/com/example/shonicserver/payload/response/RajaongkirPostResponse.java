package com.example.shonicserver.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RajaongkirPostResponse {
    private Object query;
    private Object destination_details;
    private Object origin_details;
    private Object status;
    private List<CourierApiResponse> results;

}