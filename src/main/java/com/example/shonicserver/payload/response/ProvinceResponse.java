package com.example.shonicserver.payload.response;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProvinceResponse {
    private Long id;
    private String name;

    public static ProvinceResponse of(Map<String, String> data){
        return ProvinceResponse.builder()
                .id(Long.valueOf(data.get("province_id")))
                .name(data.get("province"))
                .build();
    }

}
