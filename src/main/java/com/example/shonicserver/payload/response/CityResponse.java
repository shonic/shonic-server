package com.example.shonicserver.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import java.util.Map;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityResponse {
    private Long id;
    private String name;
    @JsonProperty("postal_code")
    private String postalCode;
    @JsonProperty("province_id")
    private Long provinceId;

    public static CityResponse of(Map<String,String> data){
        return CityResponse.builder()
                .id(Long.valueOf(data.get("city_id")))
                .name((String)data.get("type")+" "+data.get("city_name"))
                .postalCode((String) data.get("postal_code"))
                .provinceId(Long.valueOf(data.get("province_id")))
                .build();
    }


}
