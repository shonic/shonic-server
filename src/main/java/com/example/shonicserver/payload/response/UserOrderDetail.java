package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class UserOrderDetail {
    private UUID id;
    private String email;
    private String name;

    public static UserOrderDetail of(User user){
        return UserOrderDetail.builder()
                .id(user.getId())
                .email(user.getUsername())
                .name(user.getFullname())
                .build();
    }
}
