package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.Order;
import com.example.shonicserver.model.Payment;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDetailResponse {
    private Long id;
    @JsonProperty("order_number")
    private String orderNumber;
    private String date;
    private String status;
    @JsonProperty("status_detail")
    private String statusDetail;
    private String estimate;
    private String deadline;
    private ProductOrderResponse product;
    @JsonProperty("total_price")
    private Integer totalPrice;
    private Integer subtotal;
    private Integer cost;
    private Boolean reviewed;
    private String note;
    private Addresses address;
    @JsonProperty("courier_code")
    private String courierCode;
    @JsonProperty("resi")
    private String resi;
    private String payment;
    @JsonProperty("contact_admin")
    private Boolean contactAdmin;

    public static OrderDetailResponse of(Order order){
        return OrderDetailResponse.builder()
                .id(order.getId())
                .orderNumber(order.getOrderNumber())
                .date(CommonFunction.getDateOrder(order.getCreatedAt()))
                .status(order.getStatus())
                .statusDetail(order.getStatusDetail())
                .deadline(order.getDeadline())
                .estimate(order.getEstimasi())
                .totalPrice(order.getTotal())
                .product(ProductOrderResponse.of(order))
                .cost(order.getCost())
                .subtotal(order.getSubTotal())
                .reviewed(order.getReviewed())
                .note(order.getNote())
                .address(order.getAddress())
                .courierCode(order.getCourierCode())
                .resi(order.getResi())
                .payment(order.getPaymentStr())
                .contactAdmin(order.getStatus() == Order.DIBATALKAN && order.getPayment().getStatus() == Payment.KURANG)
                .build();
    }
}
