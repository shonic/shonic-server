package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Account;
import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.Order;
import com.example.shonicserver.model.Payment;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
public class AdminOrderConfirmResponse {
    @JsonProperty("order_id")
    private Long orderId;
    @JsonProperty("order_number")
    private String orderNumber;
    private String datePayment;
    @JsonProperty("total_price")
    private String totalPrice;
    private String subtotal;
    private Integer cost;
    private String note;
    private Account account;
    private String status;
    @JsonProperty("bank_number")
    private String bankNumber;
    private String name;
    private String bankName;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("image_name")
    private String imageName;
    @JsonProperty("total_transfer")
    private Integer totalTransfer;

    public static AdminOrderConfirmResponse of(Payment data){
        Order order = data.getOrder();
        return AdminOrderConfirmResponse.builder()
                .orderId(order.getId())
                .totalPrice(CommonFunction.curenyFormat(order.getTotal().doubleValue()))
                .subtotal(CommonFunction.curenyFormat(order.getSubTotal().doubleValue()))
                .cost(order.getCost())
                .bankNumber(data.getBankNumber())
                .name(data.getName())
                .bankName(data.getBankName())
                .imageUrl(data.getImageUrlStr())
                .imageName(data.getImageUrl())
                .totalTransfer(data.getTotalTransfer())
                .datePayment(CommonFunction.getDateOrder(data.getUpdatedAt()))
                .note(order.getNote())
                .status(data.getStatus())
                .orderNumber(order.getOrderNumber())
                .account(data.getAccount())
                .build();
    }
}
