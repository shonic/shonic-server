package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.Order;
import com.example.shonicserver.model.Payment;
import com.example.shonicserver.model.User;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdminOrderDetail {
    private Long id;
    @JsonProperty("order_number")
    private String orderNumber;
    @JsonProperty("date_order")
    private String date;
    private String status;
    @JsonProperty("status_detail")
    private String statusDetail;
    private String estimate;
    private String deadline;
    private ProductOrderResponse product;
    @JsonProperty("total_price")
    private Integer totalPrice;
    private String subtotal;
    private String cost;
    private Boolean reviewed;
    private String note;
    private Addresses address;
    @JsonProperty("courier_code")
    private String courierCode;
    @JsonProperty("courier_service")
    private String courierService;
    @JsonProperty("resi")
    private String resi;
    private String weight;
    private Payment payment;
    private UserOrderDetail customer;
    private String totalPriceStr;


    public static AdminOrderDetail of(Order order){
        return AdminOrderDetail.builder()
                .id(order.getId())
                .orderNumber(order.getOrderNumber())
                .date(CommonFunction.getDateOrder(order.getCreatedAt()))
                .status(order.getStatus())
                .statusDetail(order.getStatusDetail())
                .deadline(order.getDeadline())
                .estimate(order.getEstimasi())
                .totalPriceStr(CommonFunction.numberFormatWithoutComma(order.getTotal().doubleValue()))
                .totalPrice(order.getTotal())
                .product(ProductOrderResponse.of(order))
                .cost(CommonFunction.curenyFormat(order.getCost().doubleValue()))
                .subtotal(CommonFunction.curenyFormat(order.getSubTotal().doubleValue()))
                .reviewed(order.getReviewed())
                .note(order.getNote())
                .address(order.getAddress())
                .courierCode(order.getCourierCode().toUpperCase())
                .resi(order.getResi())
                .payment(order.getPayment())
                .courierService(order.getCourierService())
                .customer(UserOrderDetail.of(order.getUser()))
                .weight(String.valueOf(order.getProduct().getWeight() * order.getQty()))
                .build();

    }
}
