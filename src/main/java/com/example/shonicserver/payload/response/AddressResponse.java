package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Addresses;
import com.example.shonicserver.model.City;
import com.example.shonicserver.model.Province;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressResponse {
    private Long id;
    private String name;
    private String phone;
    private City city;
    private Province province;
    private String kecamatan;
    private String detail;
    private String postalCode;

    public static AddressResponse of(Addresses data){
        return AddressResponse.builder()
                .id(data.getId())
                .name(data.getName())
                .phone(data.getPhone())
                .city(data.getCity())
                .province(data.getProvince())
                .kecamatan(data.getKecamatan())
                .detail(data.getDetail())
                .postalCode(data.getPostalCode()).build();
    }
}
