package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Order;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
@Getter
@Setter
@Builder
public class AdminOrderProductResponse {
    private UUID id;
    private String name;
    private Integer price;
    private Integer discount;
    private Integer qty;
    private Float weight;

    public static AdminOrderProductResponse of(Order data){
        return AdminOrderProductResponse.builder()
                .id(data.getProduct().getId())
                .name(data.getProduct().getName())
                .price(data.getProduct().getPrice())
                .discount(data.getDiscount())
                .qty(data.getQty())
                .weight(data.getProduct().getWeight())
                .build();
    }
}
