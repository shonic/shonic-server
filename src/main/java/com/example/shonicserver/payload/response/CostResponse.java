package com.example.shonicserver.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CostResponse {
    @JsonProperty("courier_code")
    private String courierCode;
    @JsonProperty("courier_name")
    private String courierName;



}
