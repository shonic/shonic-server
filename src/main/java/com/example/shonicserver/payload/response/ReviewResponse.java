package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.Rating;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
public class ReviewResponse {
    @JsonProperty("total_rating")
    private Float totalRating;
    @JsonProperty("total_review")
    private Integer totalReview;
    private List<ReviewListResponse> reviews;

    public static ReviewResponse of(List<Rating> data, Product product){

        return ReviewResponse.builder()
                .totalReview(product.getReview())
                .totalRating(product.getRating())
                .reviews(data.stream().map(ReviewListResponse::of).collect(Collectors.toList()))
                .build();

    }
}
