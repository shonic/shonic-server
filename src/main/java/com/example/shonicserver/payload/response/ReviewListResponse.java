package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Rating;
import com.example.shonicserver.util.CommonFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
public class ReviewListResponse {

    @JsonProperty("review_id")
    private UUID reviewId;
    @JsonProperty("user_id")
    private UUID userId;
    @JsonProperty("user_name")
    private String userName;
    private String review;
    private Integer rating;
    private String date;

    public static ReviewListResponse of(Rating data){
        return ReviewListResponse.builder()
                .reviewId(data.getId())
                .userId(data.getUser().getId())
                .userName(data.getUser().getFullname())
                .review(data.getUlasan())
                .rating(data.getRating())
                .date(CommonFunction.getDateFrom(data.getCreatedAt(),"dd-MM-yyyy"))
                .build();
    }
}
