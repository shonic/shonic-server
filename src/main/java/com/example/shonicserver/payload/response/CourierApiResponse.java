package com.example.shonicserver.payload.response;

import lombok.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourierApiResponse {
    private String code;
    private String name;
    private List<ServiceApiResponse> costs;

//    public static CourierApiResponse of(Map<String,Object> data){
//        return CourierApiResponse.builder()
//                .code((String) data.get("code"))
//                .name(String.valueOf(data.get("name")))
//                .costs(Collections.singletonList((ServiceApiResponse)data.get("costs")))
//                .build();
//    }
}
