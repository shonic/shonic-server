package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Brand;
import com.example.shonicserver.model.Categories;
import com.example.shonicserver.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateProductResponse {
    private UUID id;
    private String name;
    private Integer price;
    private Integer qty;
    private Categories categoriesList;
    private Brand brand;
    private String image;
    private String imageFull;
    private Integer discount;
    private Float weight;
    private String description;

    public static  CreateProductResponse of(Product product){
        return CreateProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .qty(product.getQty())
                .categoriesList(product.getCategories())
                .brand(product.getBrand())
                .image(product.getImage())
                .imageFull(product.getImageFull())
                .discount(product.getDiscount())
                .weight(product.getWeight())
                .description(product.getDescription())
                .build();
    }
}
