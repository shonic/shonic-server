package com.example.shonicserver.payload.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class CostApiResponse {
    private Integer value;
    private String etd;
    private String note;
}
