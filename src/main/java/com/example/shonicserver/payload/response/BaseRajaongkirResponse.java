package com.example.shonicserver.payload.response;

import lombok.*;

@Getter
@Setter
public class BaseRajaongkirResponse<T> {
    private RajaongkirPostResponse rajaongkir;
}
