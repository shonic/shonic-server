package com.example.shonicserver.payload.response;

import com.example.shonicserver.dto.ProductListDTO;
import com.example.shonicserver.model.Brand;
import com.example.shonicserver.model.Categories;
import com.example.shonicserver.model.Product;
import com.example.shonicserver.model.ProductFlashsale;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Builder
public class ProductFlashsaleResponse {
    private UUID id;
    private LocalDateTime createdAt;
    private String image;
    private String name;
    private Integer price;
    @JsonProperty("after_discount")
    private Integer afterDiscount;
    private Integer qty;
    private Integer discount;
    private Float rating;
    private Brand brand;
    private Categories category;
    private Integer review;

    public static ProductFlashsaleResponse of(ProductFlashsale data){
        Product product = data.getProduct();
        return ProductFlashsaleResponse.builder()
                .id(product.getId())
                .createdAt(product.getCreatedAt())
                .image(product.getImage())
                .name(product.getName())
                .price(product.getPrice())
                .qty(product.getQty())
                .discount(product.getDiscount()==null? 0 : product.getDiscount())
                .rating(product.getRating() == null ? 0 : product.getRating())
                .brand(product.getBrand())
                .category(product.getCategories())
                .review(product.getReview() == null ? 0 : product.getReview())
                .afterDiscount(product.getDiscount() > 0? product.getPrice()-(product.getPrice() * product.getDiscount() / 100) : product.getPrice() )
                .build();
    }
}
