package com.example.shonicserver.payload.response;

import com.example.shonicserver.model.Order;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductOrderResponse {
    private UUID id;
    private String name;
    private Integer price;
    @JsonProperty("price_after_disc")
    private Integer priceAfterDisc;
    private Integer discount;
    private Integer qty;
    @JsonProperty("image_url")
    private String imageUrl;
    private Float weight;

    public static ProductOrderResponse of(Order data){
        return ProductOrderResponse.builder()
                .id(data.getProduct().getId())
                .name(data.getProduct().getName())
                .price(data.getProduct().getPrice())
                .priceAfterDisc(data.getDiscount() > 0? data.getProduct().getPrice()-(data.getProduct().getPrice() * data.getDiscount() / 100) : data.getProduct().getPrice())
                .discount(data.getDiscount())
                .qty(data.getQty())
                .imageUrl(data.getProduct().getImage())
                .weight(data.getProduct().getWeight())
                .build();
    }
}
